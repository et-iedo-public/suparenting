FROM python:3.7-slim
RUN mkdir -p /opt/sunagconf/
WORKDIR /opt/sunagconf/
RUN mkdir extra/
RUN apt-get update && apt-get upgrade -y && apt-get install -y curl vim less
RUN easy_install pip
RUN pip install virtualenv
ADD ./requirements.txt ./
ADD ./setup.sh ./
RUN ./setup.sh
# Now requirements.txt should be installed.
ADD ./server.py ./
ADD ./sunagconf.sh ./
ADD ./suparenting ./
ADD ./suparenting.py ./
ADD ./datastores.py ./
ADD ./datastores.yaml ./
RUN chmod a+x ./suparenting
RUN chmod a+x ./sunagconf.sh
# All of this to get env variables from docker-compose.yaml into Dockerfile
# See https://github.com/docker/compose/issues/1837#issuecomment-316896858
ARG SUPARENTING_PROD_MODE
ENV SUPARENTING_PROD_MODE "$SUPARENTING_PROD_MODE"
ENTRYPOINT ["timeout", "--kill-after", "300s", "300s", "./sunagconf.sh" ]