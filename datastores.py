#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>"
__license__ = "Apache License 2.0"
"""
    datastores.py
    ~~~~~~~~
    datastores related code, plus some support functions and exceptions
"""

import sys, os, functools, ipaddress, socket, types, objectpath, pandas, json, yaml, logging
from abc import ABC, abstractmethod

logging.basicConfig(
    stream=sys.stdout,
    level=os.environ.get("LOGLEVEL", "INFO"),
    format="%(levelname)s %(message)s",
)
logger = logging.getLogger(__name__)


class NamingResolutionError(Exception):
    pass


class NoHostError(Exception):
    pass


class UnknownExtension(Exception):
    pass


def resolve_to_ip(host):
    logger.debug("Resolving for: %s", host)
    host = host.strip()
    if host.endswith(".local"):
        return None
    try:
        ip_address = ipaddress.ip_address(host)
        logger.debug("IP passed: %s", host)
        return ip_address
    except:
        if "." not in host:
            host += ".stanford.edu"
        try:
            # the gethostbyname syscall returns a 3-ple, index 2 is array of IP(s)
            address = socket.getaddrinfo(host, None)[0][4][0]
            logger.debug("IP resolved: %s", address)
            return ipaddress.ip_address(address)
        except socket.gaierror:
            logger.debug("Can not resolve name: %s", host)
            return None


def formatted(func):
    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        data = func(*args, **kwargs)  # if the next IFs fail, data will be returned
        logger.debug(f"Formatting as {args[0].format}")
        if args[0].format == "JSON":  # args[0] being self, so it's evaluated at runtime
            engine = json.dumps
        if args[0].format == "YAML":
            engine = yaml.dump
        if isinstance(data, types.GeneratorType):
            logger.debug("Formatting a GeneratorType")
            return engine(list(data), indent=2)
        elif type(data) is dict:
            logger.debug("Formatting a dict")
            return engine(data, indent=2)
        elif (
            type(data) is pandas.core.frame.DataFrame
            or type(data) is pandas.core.series.Series
        ):
            logger.debug(f"Formatting a {type(data)}")
            return engine(data.to_dict(), indent=2)
        elif data is None:
            return ""
        else:
            return data

    return func_wrapper


class Datastore(ABC):
    def __init__(self, config):
        if not os.path.exists(config.get("path")):
            raise IOError(
                "Make sure datastore path exists! just tried to use {0}".format(
                    config.get("path")
                )
            )
        for key, value in config.items():
            setattr(self, key, value)

    @classmethod
    def factory(self, config):
        path = config.get("path")
        for c in Datastore.__subclasses__():
            if path.endswith(c.extension):
                return c(config)
        raise UnknownExtension("Unknown extension: {0}".format(path))

    @abstractmethod
    def filter(self):
        pass

    @abstractmethod
    def search(self):
        pass

    @abstractmethod
    def get_object_by_host(self):
        pass


class CSVDatastore(Datastore):
    extension = ".csv"

    def __init__(self, config):
        self.ds = self.__import_csv(config.get("path"))
        super().__init__(config)

    def __import_csv(self, path):
        logger.debug("Loading CSV {0}...".format(path))
        return pandas.read_csv(path)

    def search(self, key, value):
        return self.ds[
            self.ds[key].astype(str).str.contains(str(value) + "[^0-9]", regex=True)
        ]

    def filter(self, row, key=None):
        logger.debug(f"filtering {type(row)} with key: {key}")
        return (
            row.get(key)
            if key
            else row.reindex(self.show_columns)
            if self.show_columns
            else row
        )

    def get_object_by_host(self, host, key=None):
        ip_address = resolve_to_ip(host)
        dataset = self.search(self.network_range_field, ip_address)
        row = pandas.Series([])
        for index, row in dataset.iterrows():
            try:
                address = row.get(self.network_range_field)
                if ip_address in ipaddress.ip_network(address):
                    break
            except AttributeError:
                continue
            except ValueError:
                if str(ip_address) in address:
                    break
        if row.any():
            logger.debug(
                "Host %s matches network %s", host, row.get(self.network_range_field)
            )
            return self.filter(row, key)
        logger.debug("Host %s NOT found in any networks", host)
        return None


class JSONDatastore(Datastore):
    extension = ".json"

    def __init__(self, config):
        self.ds = self.__import_json(config.get("path"))
        super().__init__(config)

    def __import_json(self, path):
        logger.debug("Loading JSON {0}...".format(path))
        with open(path, "r") as f:
            datastore = json.load(f)
        logger.debug("{0} loaded".format(path))
        return objectpath.Tree(datastore)

    def search(self, key, value, search_string='$..*[@.{} is "{}"]'):
        return self.ds.execute(search_string.format(key, value)) or []

    def filter(self, item, key=None):
        logger.debug(f"filtering {type(item)} with key: {key}")
        return (
            item.get(key)
            if key
            else {k: v for k, v in item.items() if k in self.show_columns}
            if self.show_columns
            else item
        )

    def get_object_by_host(self, host, key=None):
        ip_address = resolve_to_ip(host)
        dataset = self.search(self.network_range_field, ip_address, self.search_string)
        for item in dataset:
            try:
                if ip_address in ipaddress.ip_network(
                    item.get(self.network_range_field)
                ):
                    logger.debug(
                        "Host %s matches network %s",
                        host,
                        item.get(self.network_range_field),
                    )
                    return self.filter(item, key)
            except AttributeError:
                continue
        logger.debug("Host %s NOT found in any networks", host)
        return None
