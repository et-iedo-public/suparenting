#!env/bin/python
from flask import Flask, request
from suparenting import *


def create_app(config="datastores.yaml"):
    app = Flask(__name__, instance_relative_config=True)

    with open(config) as f:
        DATASTORES = yaml.load(f, Loader=yaml.SafeLoader)
    parenting = NagiosParenting(DATASTORES, "JSON")

    @app.route(
        "/suparenting/api/v1.0/ds/<string:datastore>/hosts/<string:host>",
        methods=["GET"],
    )
    def get_host_info(datastore, host):
        return parenting.get_host_info(host, request.args.get("key"), datastore)

    @app.route("/suparenting/api/v1.0/ds/<string:datastore>/search", methods=["GET"])
    def search_info(datastore):
        return parenting.search_info(
            request.args.get("key"), request.args.get("value"), datastore
        )

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
