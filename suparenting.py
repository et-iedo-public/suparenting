#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>"
__license__ = "Apache License 2.0"
"""
    suparenting
    ~~~~~~~~
    Retrieve info from networking assets to craft parenting for Nagios
"""
import sys, os, ipaddress, json, yaml, argparse, logging
from datastores import *


class NagiosParenting(object):
    def __init__(self, datastores, format):
        self.format = format
        for ds, config in datastores.items():
            setattr(self, ds, Datastore.factory(config))

    @formatted
    def get_host_info(self, host, key, datastore):
        return getattr(self, datastore).get_object_by_host(host, key)

    @formatted
    def search_info(self, key, value, datastore):
        return getattr(self, datastore).search(key, value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-i", "--interactive", action="store_true", help="don't return, go into Ipython"
    )
    parser.add_argument(
        "-k",
        "--key",
        action="store",
        default=None,
        dest="key",
        help="Search key (see headers of JSON files for what to use)",
    )
    parser.add_argument(
        "-v", "--value", action="store", dest="value", help="Value of key to search for"
    )
    parser.add_argument(
        "-F",
        "--format",
        action="store",
        default="JSON",
        dest="format",
        help="Preferred output format",
    )
    parser.add_argument(
        "-D",
        "--datastore",
        action="store",
        default="vcenter",
        dest="datastore",
        help="What datasource to look into, according to datastore_yaml config file",
    )
    parser.add_argument(
        "--datastore_yaml",
        action="store",
        dest="datastore_yaml",
        default="datastores.yaml",
        type=argparse.FileType(),
        help="Datastore config file path in YAML format",
    )
    parser.add_argument(
        "-H",
        "--host",
        action="store",
        dest="host",
        help="Find network segment by host (IP or hostname)",
    )
    args = parser.parse_args()
    if len(sys.argv) < 2:
        parser.print_usage()
        sys.exit(1)

    DATASTORES = yaml.load(args.datastore_yaml, Loader=yaml.SafeLoader)
    logger.debug(f"Formatting output with {args.format}")
    parenting = NagiosParenting(DATASTORES, args.format)

    if args.key and args.value:
        print(parenting.search_info(args.key, args.value, args.datastore))
    elif args.host:
        print(parenting.get_host_info(args.host, args.key, args.datastore))
    if args.interactive:
        import IPython  # import IPython; IPython.embed()

        IPython.embed()
