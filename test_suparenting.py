#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pytest
from suparenting import *

with open("tests/test_datastores.yaml", "r") as ymlfile:
    DATASTORES = yaml.load(ymlfile, Loader=yaml.SafeLoader)
tp = NagiosParenting(DATASTORES, "JSON")


def test_suparenting_lookup_vlan():
    assert len(tuple(tp.net2oa.search("vlan", 920))) == 1


def test_suparenting_lookup_vsys_by_hostname():
    assert tp.get_host_info("nagios02", "vsys", "net2oa") == "33"


def test_suparenting_lookup_vsys_by_ipaddress():
    assert tp.get_host_info("171.67.217.114", "vsys", "net2oa") == "33"


def test_get_object_no_results():
    assert tuple(tp.net2oa.search("firewall", "somethinginexistent")) == ()


def test_get_object_one_result():
    assert len(tuple(tp.net2oa.search("vlan", 1001))) == 1


def test_get_object_multiple_results():
    assert len(tuple(tp.net2oa.search("op_area", "noa"))) > 4


def test_get_host_info():
    rs = tp.get_host_info("171.67.217.114", None, "vcenter")
    json.loads(rs)  # If it raises exception, it's not a valid JSON
    assert len(rs) > 500  # Some random high number to show we received it.
