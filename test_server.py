#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pytest
from server import create_app
from flask import url_for


@pytest.fixture
def app():
    app = create_app(config="tests/test_datastores.yaml")
    return app


def test_get_host_info_net2oa_monitoring(client):
    response = client.get(
        url_for("get_host_info", datastore="net2oa", host="monitoring")
    )
    assert response.status_code == 200
    assert b"920" in response.data


def test_get_host_info_net2oa_monitoring_with_key(client):
    response = client.get(
        url_for("get_host_info", datastore="net2oa", host="monitoring", key="vsys")
    )
    assert response.status_code == 200
    assert b"33" in response.data


def test_search_info_net2oa(client):
    response = client.get(
        url_for("search_info", datastore="net2oa", key="vsys", value="33")
    )
    assert response.status_code == 200
    assert b"fw1" in response.data
