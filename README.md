# sunagconf and suparenting

Craft the hierarchical parenting json file needed by Nagios to determine interdependencies: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/networkreachability.html

There are 2 scripts, sunagconf (to manipulate Nagioc config) and suparenting, which is used by sunagconf, and does all the lookups through Networking's JSON files and vCenter dumps. The latter can be used interactively and for far beyond sunagconf's needs.  See the usage and following examples.

# Getting started
```
git clone https://code.stanford.edu/et-iedo/suparenting
cd suparenting/
./setup.sh
echo Copy into extra/ the JSONs as per the locations in the section below
cp /path/to/yue/jsons/* extra/data
./validate.sh # This will build the docker container as well
```
For the main sunagconf tool, usage is as follows:
```
USAGE: ./sunagconf.sh  [-c CONFDIR]
  -c CONFDIR  Directory where the Nagios configs are
  -b          Build parenting structure by resolving parents and updating config files
  -l          List all hosts defined within the configuration directory passed
  -G          List all hostgroups defined within the configuration directory passed
  -n          If no lookup was successful for the node, don't delete the current parents attr if present
  -u          If an existing parents attr is there, update it
  -S          Speed up resolution by starting a flask server, thus avoiding constant reimports of Datasources
```
Then you can look at the usage of the lookup tool with ./suparenting.py -h:
```
golfieri@MacBook-Pro-7:~/work/suparenting$ ./suparenting -h
usage: suparenting.py [-h] [-i] [-k KEY] [-v VALUE] [-F FORMAT] [-D DATASTORE]
                      [--datastore_yaml DATASTORE_YAML] [-H HOST]

optional arguments:
  -h, --help            show this help message and exit
  -i, --interactive     don't return, go into Ipython (default: False)
  -k KEY, --key KEY     Search key (see headers of JSON files for what to use)
                        (default: None)
  -v VALUE, --value VALUE
                        Value of key to search for (default: None)
  -F FORMAT, --format FORMAT
                        Preferred output format (default: JSON)
  -D DATASTORE, --datastore DATASTORE
                        What datasource to look into, according to
                        datastore_yaml config file (default: vcenter)
  --datastore_yaml DATASTORE_YAML
                        Datastore config file path in YAML format (default:
                        datastores.yaml)
  -H HOST, --host HOST  Find network segment by host (IP or hostname)
                        (default: None)
```
You wanna take a look at the datastores.yaml file which should be self-explanatory:
```
net2oa:
  path: extra/data/net2oa.json
  network_range_field: addressSpace
  search_string: $.addressSpaces.*
  show_columns:
    - firewall
    - vlan
    - addressSpace
    - activeIPCount
    - OA
...
```
# Datasource files
You might need to ask for permission to get those json files. Ask Yue Lu.
```
https://drive.google.com/drive/u/1/folders/1eYieP8JCUMHeHaYcRSSt7dLt2YYuQ_ID
```
# Usage Examples
```
$ ./suparenting -H monitoring

{
  "Name": "Nagios02 - Nagios 4.3.4",
  "CPUs": 4,
  "State": "Powered On",
  "Status": "Normal",
  "Provisioned Space": "392.47  GB",
  "Used Space": "298.74  GB",
  "Host CPU": 544,
  "Host Mem": 8067,
  "VM Storage Policies Compliance": NaN,
  "Managed By": NaN,
  "Host": "asxuprd13.stanford.edu",
  "Host Type": "ESXi",
  "Guest Mem - %": 32,
  "Guest OS": "Other Linux (64-bit)",
  "Compatibility": "ESXi 5.0 and later (VM version 8)",
  "Memory Size": "7.84  GB",
  "Reservation": "0  B",
  "NICs": 1,
  "Uptime": "15 days",
  "IP_Address": "171.67.207.114, fe80::250:56ff:fe85:31ab",
  "VMware Tools Version Status": "guestToolsUnmanaged|2147483647",
  "VMware Tools Running \n": "Running",
  "DNS Name": "nagios02.stanford.edu",
  "EVC Mode": "Intel\u00ae \"Nehalem\" Generation",
  "UUID": "42173dfd-4e43-93c0-64ea-b54598cd81db",
  "Notes": NaN,
  "Alarm Actions": "Enabled",
  "HA Protection": "Protected",
  "Needs Consolidation": "Not Required",
  "Encryption": "No",
  "Datacenter": "Forsythe",
  "Cluster": "ESX_PRD",
  "Shares Value": NaN,
  "Limit - IOPs": NaN,
  "Datastore % Shares": NaN
}
==========================================================
$ ./suparenting -H monitoring -k vlan -D net2oa

920
==========================================================
$ ./suparenting -H 192.168.20.1 -D net2oa

{
  "addressSpace": "192.168.16.0/21",
  "activeIPCount": 0,
  "OA": "noa",
  "firewall": "nc-srtr",
  "vlan": "581",
  "zone": "cons-ndcch",
  "vsys": "95",
  "buildings": [
    "14-696 Northwest Data Center - NDCCH"
  ]
}
==========================================================
$ ./suparenting -k 'addressSpace' -v '204.63.231.160/27' -D net2oa

[
  {
    "addressSpace": "204.63.231.160/27",
    "activeIPCount": 2,
    "OA": "loa",
    "firewall": "loa-srtr",
    "vlan": "114",
    "zone": "reg-webapp",
    "vsys": "7",
    "buildings": [
      "90-615 475 Longfellow Ct"
    ]
  }
]
==========================================================
$ ./suparenting -k 'vlan' -v '920' -D net2oa

[
  {
    "addressSpace": "171.67.207.112/28",
    "activeIPCount": 3,
    "OA": "dc2",
    "firewall": "dc2-srtr",
    "vlan": "920",
    "zone": "trust-unixmon",
    "vsys": "33",
    "buildings": [
      "14-200 Forsythe Hall, 275 Panama"
    ]
  },
  {
    "addressSpace": "172.27.207.112/28",
    "activeIPCount": 0,
    "OA": "dc2",
    "firewall": "dc2-srtr",
    "vlan": "920",
    "zone": "trust-unixmon",
    "vsys": "33",
    "buildings": [
      "14-200 Forsythe Hall, 275 Panama"
    ]
  }
]
```
# sunagconf
Script to interact with Nagios config. You can list all active hosts configured, but also update the nagios configs. See -h option for full usage. Only run locally on your computer, DO NOT run on the prod server directly. For example, to list all hosts active in nagios:
```
(env) golfieri@sr18-181b3cdd5c:~/work/suparenting$ ./sunagconf -c extra/nagios-config/ -l |  head
its-exchmb03
its-exchfe03
its-exch-dpm01
owa
kerberos1
kerberos2
kerberos3
kdc-prod1
kdc-prod2
kdc-prod3
```

# VScode launch.json
```
        {
            "name": "Py: suparenting",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "cwd": "${workspaceFolder}/suparenting",
            "console": "integratedTerminal",
            "args" : ["-H", "monitoring"],
            "env": {
                "LOGLEVEL": "DEBUG"
            },
        },
```

