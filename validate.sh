#!/usr/bin/env bash
set -e
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${WORKDIR}
source env/bin/activate
pip3.7 freeze > requirements.txt 
black -v *.py
pytest
# ./demo.sh "$@"
[[ "$(whoami)" == "gitlab-runner" ]] && SUDO=sudo
${SUDO} docker build . -t sunagconf:latest
