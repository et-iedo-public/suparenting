Nagios config root: /usr/local/nagios4/etc/conf.d (or ssh://gitolite@git.stanford.edu/server/nagios-config)

Host example 1:
```
define host {
    use         its-test-template
    host_name   dreamingbuck
    alias       dreamingbuck
    address     171.64.2.139
}
```
That host uses the its-test-template Host Template which uses smarts-test1 as a contact
```
define host {
    name                    its-test-template
    use                     default-host-template
    check_interval          1
    retry_interval          1
    max_check_attempts    2
    notification_interval   0
    contacts              smarts-test1
    hostgroups              its-test-hostgroup
    register                0
}
```
And the contact sets an email address
```
define contact {
    contact_name    smarts-test1
    alias           smarts-test1
    use             email-contact-template
    email           smarts@smarts-test1.stanford.edu
}
```
Host example 2:
```
define host {
    use         sophos-host-template
    host_name   sophosupdates
    alias       sophosupdates
    address     sophosupdates.stanford.edu
}
```
That host uses the sophos-host-template Host Template which uses the iso-secops-contactgroup
```
define host {
    name                    sophos-host-template
    use                     default-host-template
    contact_groups          iso-secops-contactgroup
    register                0
}
```
The contactgroup combines multiple contacts:
```
define contactgroup {
    contactgroup_name    iso-secops-contactgroup
    alias                iso-secops-contactgroup
    members              list-securityops-alerts,jtavan-email,sbl-email,sknayar-email,tsimpson-email,bobbyg-email,ywillis-email
}
```
Here is one of the contacts in the list in the same format as smarts-test1:
```
define contact {
    contact_name    list-securityops-alerts
    alias           SecOps Alerts
    use             email-contact-template
    email           securityops-alerts@lists.stanford.edu
    pager           securityops-alerts@lists.stanford.edu
}
```
All of these config elements can exist in multiple files all either in the root config directory or some other subdirectory.  

I also found this which appears to be pretty fully featured.  Maybe it will suit our needs here as well as for parenting (since it can write to the configs as well)

https://github.com/pynag