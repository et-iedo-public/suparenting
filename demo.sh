#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${WORKDIR}
source env/bin/activate
IFS=$'\n'
DEMO_STEPS=$(cat <<END
./suparenting -H nagios02
./suparenting -H INEXISTENT
./suparenting -H monitoring -k vlan -D net2oa
./suparenting -H 192.168.20.1 -D net2oa
./suparenting -k 'address_space' -v '204.63.231.160/27' -D net2oa
./suparenting -k 'vlan' -v '920' -D net2oa
END
)
while read -r line; do
    echo ==========================================================
    echo $ $line "$@"
    echo
    eval "$line" "$@"
done <<< "${DEMO_STEPS}"
