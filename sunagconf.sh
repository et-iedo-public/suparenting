#!/usr/bin/env bash
# __author__ = 'UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>'
# __license__ = 'Apache License 2.0'  
# """
#     Script to interact with Nagios config files
# """

CURRENTDIR=${PWD}
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${WORKDIR}
IFS=$'\n'
trap "kill 0" EXIT # So that flask gets killed as well if running.
UPDATE_PARENT=false
DELETE_PARENT=false
FLASK_SERVER=false
SED_BIN=gsed
GUNICORN_WORKERS=5
# "Running in container, better be linux. Otherwise most prob is Mac OS X"
[[ -f /.dockerenv ]] && SED_BIN=sed || SED_BIN=gsed
# Otherwise we won't see gunicorn installed by pip

source env/bin/activate
usage () {
  cat << EOF
USAGE: sunagconf  [-c CONFDIR]
  -c CONFDIR  Directory where the Nagios configs are
  -b          Build parenting structure by resolving parents and updating config files
  -l          List all hosts defined within the configuration directory passed
  -G          List all hostgroups defined within the configuration directory passed
  -D          If lookup was not successful for the node,  delete the current parents attr if present
  -u          If an existing parents attr is there, update it
  -S          Speed up resolution by starting a flask server, thus avoiding constant reimports of Datasources
  -w          Gunicorn workers, default 5
EOF
  exit 1;
}

list_groups() {
  for FILE in $(find ${CONFDIR} -name '*.cfg'); do
    for LINE in $(cat ${FILE} | pcregrep -M -n "(?s)^define hostgroup\s+{[^{]*hostgroup_name[^}]*}" | grep 'hostgroup_name' | grep -v '#'); do
      HOST=$(echo ${LINE} |awk '{print $2}')
      echo ${HOST}
    done
  done
}

list_hosts() {
  for FILE in $(find ${CONFDIR} -name '*.cfg'); do
    for LINE in $(cat ${FILE} | pcregrep -M -n "(?s)^define host\s+{[^{]*host_name[^}]*}" | grep 'host_name' | grep -v '#'); do
      HOST=$(echo ${LINE} |awk '{print $2}')
      echo ${HOST}
    done
  done
}

build_config() {
  >&2 echo UPDATE_PARENT=${UPDATE_PARENT}
  >&2 echo DELETE_PARENT=${UPDATE_PARENT}
  [[ "${FLASK_SERVER}" = true ]] && start_flask_server
  for FILE in $(find ${CONFDIR} -name '*.cfg'); do
    for LINE in $(cat ${FILE} | pcregrep -M -n "(?s)^define host\s+{[^{]*host_name[^}]*}" | grep 'host_name' | grep -v '#'); do
      HOST=$(echo ${LINE} |awk '{print $2}')
      HOST=${HOST%$'\r'}
      if [ "${FLASK_SERVER}" = true ]; then
        URL="http://localhost:8000/suparenting/api/v1.0/ds/net2oa/hosts/${HOST}?key=firewall"
        PARENT=$( curl -s --unix-socket /tmp/_INTRA_.sock "${URL}" )
      else
        PARENT=$( ./suparenting -D net2oa -k firewall -H ${HOST} )
      fi
      BLOCK_MATCH=$( cat ${FILE} | pcregrep -M -n "(?s)^define host\s+{[^{]*\s${HOST}.?\s*\n\s*[^}]*}" )
      LINE_NUMBER=$( echo ${BLOCK_MATCH} | head -n1 | cut -d':' -f1 )
      if [ "${PARENT}" = "" -a "${DELETE_PARENT}" ]; then
        # Couldn't look parent up, so removing parents attribute (better be consistent than democratic)
        >&2 echo ${HOST}: REMOVE @ ${FILE}:${LINE_NUMBER}
        ${SED_BIN} -i -e "${LINE_NUMBER},/\}/{;/parents\s\+.*$/d;}" ${FILE}
      else
        ( echo ${BLOCK_MATCH} | grep -q parents )
        if [ $? -eq 1 ]; then
          # No active parents attr detected, so we go ahead and place ours at around ${LINE_NUMBER}
          >&2 echo ${HOST}: ADD ${PARENT} @ ${FILE}:${LINE_NUMBER}
          ${SED_BIN} -i -e "$((${LINE_NUMBER}+1))i \ \ \ \ parents     ${PARENT}" ${FILE}
        else
          # An uncommented parents attr was in place so we are now going to comment it and add ours
          [[ "${UPDATE_PARENT}" ]] && {
            >&2 echo ${HOST}: UPDATE ${PARENT} @ ${FILE}:${LINE_NUMBER};
            ${SED_BIN} -i -e "${LINE_NUMBER},/\}/{;s/^\(\s\+parents\s\+\).*$/#&\n\1${PARENT}/g;}" ${FILE}; }
        fi
      fi
    done
  done
  pkill -P $$  # To kill the flask server as well, if started
}

start_flask_server() {
  gunicorn -w ${GUNICORN_WORKERS}  -b unix:/tmp/_INTRA_.sock "server:create_app()" &
  sleep 3
}

while getopts "h?c:DuSw:lGb" opt; do # Order of options is important to load flags before build command
  case "$opt" in
    h|\?) usage ;;
    c)  if [[ "${OPTARG}" = /* ]]; then
          CONFDIR="${OPTARG}"
        else
          CONFDIR=${CURRENTDIR}/${OPTARG}
        fi
      [[ -d ${CONFDIR} ]] || { echo "Directory path not valid: ${CONFDIR}"; exit 1; };;
    D) declare -x DELETE_PARENT=true ;;
    u) declare -x UPDATE_PARENT=true ;;
    S) declare -x FLASK_SERVER=true ;;
    w) declare -x GUNICORN_WORKERS=${OPTARG} ;;
    l) list_hosts ;;
    G) list_groups ;;
    b) EXEC_BUILD_CONFIG=true ;;
  esac
done
[ $OPTIND == 1 ] && usage
shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

[[ ${EXEC_BUILD_CONFIG} ]] && { build_config ${UPDATE_PARENT} ${NO_LOOKUP_NO_PARENT} ${FLASK_SERVER}; };