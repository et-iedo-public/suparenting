#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
PROJECT=suparenting
cd $WORKDIR

PACKAGES='git pcregrep procps'

[[ -n "`which sudo`" ]] && SUDO=sudo || SUDO=''
if [ -n "`which apt-get`" ]; then
	$SUDO apt-get -y install ${PACKAGES}
elif [ -n "`which yum`" ]; then
	$SUDO yum -y install ${PACKAGES}
else
	echo not a debian or rh based distro, boo
	exit 1
fi
pip install virtualenv
VENV=$(which virtualenv)

if [ ! -f "$VENV" ]; then
	echo "virtualenv command not found, please install: pip install virtualenv"
	exit 1
fi

# For the main suparenting.py
$VENV -p python3 env
source env/bin/activate
pip3.7 install --upgrade pip
pip3.7 install -r requirements.txt
deactivate
echo Installation done, \"./suparenting -h\" for usage